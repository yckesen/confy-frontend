export const environment = {
  production: true,

  confyBackendUrl: 'http://confy-backend.minikube',
  keycloakAuthUrl: 'http://keycloak.minikube/auth/realms/confy'
};
