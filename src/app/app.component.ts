import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {map, shareReplay, tap} from "rxjs/operators";
import {AuthConfig, NullValidationHandler, OAuthService} from "angular-oauth2-oidc";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'confy-frontend';

  claims : object;
  roles: string[];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private oauthService: OAuthService, private router: Router, private http: HttpClient) {
    this.configure();
  }

  authConfig: AuthConfig = {
    issuer: environment.keycloakAuthUrl,
    redirectUri: window.location.origin,
    clientId: 'pac-frontend',
    //scope: 'profile email',
    responseType: 'code',
    disableAtHashCheck: true,
    showDebugInformation: true,
    requireHttps: false
  }

  public login() {
    console.log("login");
    this.oauthService.initLoginFlow();
    this.oauthService.setupAutomaticSilentRefresh();
  }

  public logout() {
    this.oauthService.logOut();
  }

  private configure() {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    this.oauthService.setStorage(localStorage)
  }

  public getName() : any {
    let claims = this.oauthService.getIdentityClaims();
    if (!claims) return null;
    return claims["given_name"];
  }

  showUser() {
    this.oauthService.loadUserProfile().then(value => console.log("userPROFILE:"+JSON.stringify(value)));
    console.log(this.getName());
    console.log("claims:"+ JSON.stringify(this.oauthService.getIdentityClaims()));
  }
}
