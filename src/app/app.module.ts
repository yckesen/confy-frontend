import { BrowserModule } from '@angular/platform-browser';
import { NgModule , LOCALE_ID} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';


import {MatSliderModule} from "@angular/material/slider";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";

import { FlexLayoutModule } from '@angular/flex-layout';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import { EventListComponent } from './component/event-list/event-list.component';
import { EventsComponent } from './component/events/events.component';
import {MatCardModule} from "@angular/material/card";
import { LayoutModule } from '@angular/cdk/layout';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {CdkTableModule} from "@angular/cdk/table";
import {MatChipsModule} from "@angular/material/chips";
import {MatTabsModule} from "@angular/material/tabs";
import { HomeComponent } from './component/home/home.component';
import { PeopleComponent } from './component/people/people.component';
import { TalkComponent } from './component/talk/talk.component';
import { TopicsComponent } from './component/topic/topics.component';
import { TopicComponent } from './component/topic/topic.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import { OAuthModule } from 'angular-oauth2-oidc';
import { PersonComponent } from './component/person/person.component';
import {environment} from "../environments/environment";


@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventsComponent,
    HomeComponent,
    PeopleComponent,
    TalkComponent,
    TopicsComponent,
    TopicComponent,
    PersonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatToolbarModule,
    MatIconModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatInputModule,
    LayoutModule,
    MatGridListModule,
    MatExpansionModule,
    MatButtonToggleModule,
    CdkTableModule,
    MatChipsModule,
    MatTabsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSelectModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [environment.confyBackendUrl+'/api/topics'],
        sendAccessToken: true
      }
    })
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'en-US' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
