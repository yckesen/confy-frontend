import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Talk} from "../model/talk";
import {Topic} from "../model/topic";
import {catchError} from "rxjs/operators";
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  postTopicSuccess: boolean;

  private topicsUrl: string;

  constructor(private http: HttpClient) {
    this.topicsUrl = environment.confyBackendUrl+'/api/topics';
  }

  public findTopic(topicId :bigint): Observable<Topic> {
    return this.http.get<Topic>(this.topicsUrl +"/"+ topicId);
  }

  public findAll(): Observable<Topic[]> {
    return this.http.get<Topic[]>(this.topicsUrl);
  }


  public createTopic(topic :Topic): Observable<Topic>{

      console.log(JSON.stringify(topic))
      let test : Topic ;
      this.http.post<Topic>(environment.confyBackendUrl+"/api/topics", topic, httpOptions)
        .subscribe({
          next: data => {
            test = data
          },
          error: error => {
            console.error('Error caught posting topic:', error);
            console.error('success eval:', error == null)

          }
        })

      return null;
    }






}
