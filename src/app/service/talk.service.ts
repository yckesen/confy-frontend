import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Person} from "../model/person";
import {Talk} from "../model/talk";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TalkService {

  private talkUrl: string;

  constructor(private http: HttpClient) {
    this.talkUrl = environment.confyBackendUrl+'/api/talks/';
  }

  public findTalk(talkId :bigint): Observable<Talk> {
    return this.http.get<Talk>(this.talkUrl+talkId);
  }
}
