import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventListComponent } from "./component/event-list/event-list.component";
import { EventsComponent } from './component/events/events.component';
import {HomeComponent} from "./component/home/home.component";
import {PeopleComponent} from "./component/people/people.component";
import {TalkComponent} from "./component/talk/talk.component";
import {TopicsComponent} from "./component/topic/topics.component";
import {PersonComponent} from "./component/person/person.component";



const routes: Routes = [
  { path: 'events', component: EventsComponent},
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent},
  { path: 'person', component: PersonComponent},
  { path: 'people', component: PeopleComponent},
  { path: 'topics', component: TopicsComponent},
  { path: 'talks/:talkId', component: TalkComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
