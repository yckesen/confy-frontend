import {EventService} from "../../../service/event.service";
import {Event} from "../../../model/event";
import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {Talk} from "../../../model/talk";
import {formatDate} from "@angular/common";

export class EventDatasource implements DataSource<Talk> {

  private loadingSubject = new BehaviorSubject<boolean>(false);
  private talkSubject = new BehaviorSubject<Talk[]>([]);

  private dateFormat = "yyyymmdd";
  private locale = "en-US";
  public loading$ = this.loadingSubject.asObservable();

  constructor(private eventService: EventService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<Talk[]> {
    return this.talkSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.talkSubject.complete();
    this.loadingSubject.complete();
  }

  loadTalks(event: Event, day: Date) {
    this.loadingSubject.next(true);

    let talkArrayByDay = event.talks.filter( talk =>
     formatDate(day, this.dateFormat,this.locale ) == formatDate(talk.startDateTime, this.dateFormat, this.locale)
    )

    let talkList = of(talkArrayByDay);
    talkList.pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(talks => this.talkSubject.next(talks));
  }
}
