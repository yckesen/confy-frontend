import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { Event } from "../../model/event";
import { ActivatedRoute, Router } from "@angular/router";
import { EventService } from "../../service/event.service";
import {EventDatasource} from "./datasource/event-datasource";
import {CdkTextColumn} from "@angular/cdk/table";
import {Talk} from "../../model/talk";
import {Room} from "../../model/room";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit, OnChanges {

  @Input() day :Date;
  @Input() event :Event;

  startTimeColumnName :string;
  endTimeColumnName :string;

  dataSource: EventDatasource;
  displayedColumns: string[];

  constructor(private eventService: EventService ) {
    this.startTimeColumnName = "Start";
    this.endTimeColumnName = "End";
    this.displayedColumns = [];
  }

  getDay() :Date {
    return this.day;
  }

  getTime(time: Date) : string {
    return formatDate(time, "HH:mm", "en-US");

  }

  getLevelIcon(level): string{
    if(level == "BEGINNER")
      return "star_border";
    if(level == "ADVANCED")
      return "star_half";
    if(level == "EXPERT")
      return "star";
    return "star"
  }

  getLevelText(level): string{
    if(level == "BEGINNER")
      return "Beginner";
    if(level == "ADVANCED")
      return "Advanced";
    if(level == "EXPERT")
      return "Expert";
    return ""
  }

  loadData(){
    this.displayedColumns = [];
    this.displayedColumns.push(this.startTimeColumnName);
    this.displayedColumns.push(this.endTimeColumnName);
    let roomNames :string[] = this.event.location.rooms.map((room: Room) => room.name);
    roomNames.forEach(val => this.displayedColumns.push( val));
    console.log(this.displayedColumns);

    this.dataSource = new EventDatasource(this.eventService);
    this.dataSource.loadTalks(this.event, this.getDay());
  }


  ngOnInit(): void {
    this.loadData();

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadData();
  }

}
