import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Talk} from "../../model/talk";
import {Person} from "../../model/person";
import {TalkService} from "../../service/talk.service";

@Component({
  selector: 'app-talk',
  templateUrl: './talk.component.html',
  styleUrls: ['./talk.component.scss']
})
export class TalkComponent implements OnInit, OnChanges {

  talkId :bigint;
  private sub: any;

  talk :Talk;


  constructor(private route: ActivatedRoute, private talkService :TalkService ) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.talkId = params['talkId'];
      this.loadTalk(this.talkId);
      // In a real app: dispatch action to load the details here.
    });



  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadTalk(this.talkId);
  }


  loadTalk(talkId: bigint){
    this.talkService.findTalk(talkId).subscribe(talk => {
      this.talk = talk as Talk
    })
  }

  getLevelIcon(level): string{
    if(level == "BEGINNER")
      return "star_border";
    if(level == "ADVANCED")
      return "star_half";
    if(level == "EXPERT")
      return "star";
    return "star"
  }

  getLevelText(level): string{
    if(level == "BEGINNER")
      return "Beginner";
    if(level == "ADVANCED")
      return "Advanced";
    if(level == "EXPERT")
      return "Expert";
    return ""
  }

}
