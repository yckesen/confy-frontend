import { Component, OnInit } from '@angular/core';
import {Event} from "../../model/event";
import {EventService} from "../../service/event.service";
import {PersonService} from "../../service/person.service";
import {Person} from "../../model/person";
import {Router} from "@angular/router";

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  persons: Person[];

  constructor(private personService: PersonService, private router: Router ) {
    this.getPersons();
  }

  onLoadTalk(talkId :bigint) {
    this.router.navigate(['/talks',talkId])
      .then(success => console.log('navigation success?' , success))
      .catch(console.error);
  }

  ngOnInit(): void {

  }


  getPersons(){
    this.personService.findAll().subscribe(persons => {
      this.persons = persons as Person[]
    })
  }

  getLevelIcon(level): string{
    if(level == "BEGINNER")
      return "star_border";
    if(level == "ADVANCED")
      return "star_half";
    if(level == "EXPERT")
      return "star";
    return "star"
  }

  getLevelText(level): string{
    if(level == "BEGINNER")
      return "Beginner";
    if(level == "ADVANCED")
      return "Advanced";
    if(level == "EXPERT")
      return "Expert";
    return ""
  }
}
