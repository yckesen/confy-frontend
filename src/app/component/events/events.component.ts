import { Component, OnInit } from '@angular/core';
import {EventService} from "../../service/event.service";
import {EventDatasource} from "../event-list/datasource/event-datasource";
import {Observable} from "rxjs";
import {Event} from "../../model/event";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events: Event[];

  isLoadDayComponent : boolean = false;

  constructor(private eventService: EventService ) {
    this.getEvents();
  }

  getEvents(){
    this.eventService.findAll().subscribe(events => {
      this.events = events as Event[]
    })
    console.log(this.events);
  }

  getLevelIcon(level): string{
    if(level == "BEGINNER")
      return "star_border";
    if(level == "ADVANCED")
      return "star_half";
    if(level == "EXPERT")
      return "star";
    return "star"
  }

  getLevelText(level): string{
    if(level == "BEGINNER")
      return "Beginner";
    if(level == "ADVANCED")
      return "Advanced";
    if(level == "EXPERT")
      return "Expert";
    return ""
  }

  ngOnInit(): void {

  }

  loadDayComponent() {
    this.isLoadDayComponent = !this.isLoadDayComponent;
  }
}
