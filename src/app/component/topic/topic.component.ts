import {Component, ElementRef, forwardRef, Inject, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Topic} from "../../model/topic";
import {ActivatedRoute} from "@angular/router";
import {TopicService} from "../../service/topic.service";
import {TopicsComponent} from "./topics.component";
import {FormControl} from "@angular/forms";
import {MatAutocomplete} from "@angular/material/autocomplete";

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  @Input() topicId : bigint;
  topic : Topic;

  topicsComponent : TopicsComponent;

  constructor(private route: ActivatedRoute, private topicService : TopicService , @Inject(forwardRef(() => TopicsComponent)) private _parent:TopicsComponent) {

  }

  ngOnInit(): void {
    this.topic = new Topic();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadTopic(this.topicId);

  }

  loadTopic(topicId: bigint){
    this.topicService.findTopic(topicId).subscribe(topic => {
      this.topic = topic as Topic
    })
  }

  changeSelected(topic: Topic) {
    this._parent.changeSelected(topic);
  }
}
