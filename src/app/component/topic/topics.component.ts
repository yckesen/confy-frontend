import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Talk} from "../../model/talk";
import {Person} from "../../model/person";
import {TalkService} from "../../service/talk.service";
import {Topic} from "../../model/topic";
import {TopicService} from "../../service/topic.service";
import {FormBuilder, FormControl, FormGroup, FormsModule, Validators} from "@angular/forms";

@Component({
  selector: 'app-topics',
  templateUrl: '../topic/topics.component.html',
  styleUrls: ['../topic/topics.component.scss']
})
export class TopicsComponent implements OnInit, OnChanges {

  private sub : any;

  topicId : bigint;
  topics : Topic[];
  selectedTopic :Topic;

  formTopic = new Topic();

  //topicForm: FormGroup;
  topicFormControl = new FormControl();

  changeSelected(topic: Topic) {
    this.selectedTopic = topic;
  }

  constructor(private route: ActivatedRoute, private topicService : TopicService, private formBuilder: FormBuilder ) {
  }

  ngOnInit(): void {

    this.sub = this.route.params.subscribe(params => {
      this.loadTopics();
    });


  }


  prepareTopicForPost(topic: Topic){

    topic.children.forEach( topic => this.resetTopicRelations(topic))
    topic.parents.forEach( topic => this.resetTopicRelations(topic))

  }

  resetTopicRelations(topic: Topic){

    topic.children = [];
    topic.parents = [];

  }

  submit() {

    this.prepareTopicForPost(this.formTopic);
    this.topicService.createTopic(this.formTopic);
    this.loadTopics();
    this.ngOnChanges(null);
    this.formTopic = new Topic();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadTopics();
  }


  loadTopics(){
    this.topicService.findAll().subscribe(topics => {
      this.topics = topics as Topic[]
    })
  }



}
