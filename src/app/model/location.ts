import {Room} from "./room";

export class Location {
  id: bigint;
  name: string;
  rooms: Room[];
}
