export class Topic {
  id: bigint;
  public name: string;
  public parents: Topic[];
  public children: Topic[];
}
