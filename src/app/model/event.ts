import {Talk} from "./talk";
import {Location} from "./location";

export class Event {
  id: bigint;
  name: string;
  startDate: Date;
  endDate: Date;
  location: Location;
  talks: Talk[];
  days: Date[];
}
