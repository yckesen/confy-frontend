import {Talk} from "./talk";
import {Organization} from "./organization";

export class Person {
  id: bigint;
  name: string;
  organization: Organization;
  talks: Talk[];
}
