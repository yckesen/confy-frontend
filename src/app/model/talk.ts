import {Topic} from "./topic";
import {Room} from "./room";
import {Time} from "@angular/common";
import {Person} from "./person";
import {Event} from "./event";

export class Talk {
  id: bigint;
  name: string;
  startDateTime: Date;
  endDateTime: Date;
  language: string;
  level: string;
  room: Room;
  event: Event;
  topics: Topic[];
  persons: Person[];

}
